var Person = function (firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
};

Person.prototype.sayHello = function () {
  var helloMessage = 'Hello ' + this.firstName + ' ' + this.lastName;
  var helloElement = document.createTextNode(helloMessage);
  document.getElementById('hello-message').appendChild(helloElement);
};

var validateUserInput = function (text) {
  if (text === null) {
    return false;
  }
  text = text.replace(/^\s+|\s+$/gm, "");
  return (text.length !== 0);
};

var getUserInput = function (message) {
  var userInput = prompt(message);
  if (!validateUserInput(userInput)) {
    userInput = getUserInput(message);
  }
  return userInput;
};

var firstName = getUserInput('Please enter your first Name');
var lastName = getUserInput('Please enter your last Name');

var person = new Person(firstName, lastName);
this.addEventListener('load', person.sayHello);
